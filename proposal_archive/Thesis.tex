%%%%
%% This Source Code Form is subject to the terms of the MIT License. 
%% If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/mit
%%
%% Last update: 2021/10/11
%% 
%% author: Dorian Gouzou <jackred@tuta.io>
%% repository hosted on github at https://github.com/jackred/Heriot_Watt_Thesis_Template
%%%%
\documentclass[12pt,a4paper]{report}

\usepackage{graphicx} % include graphics
\usepackage{fancyhdr} % layout
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc} % font
\usepackage{setspace} % spacing
% \usepackage[left=4cm,right=2cm,top=2cm,bottom=2cm]{geometry}
\usepackage{mathptmx} % looks like times new roman
\usepackage{slantsc}
\usepackage{titlesec}
\usepackage{mfirstuc}
\usepackage{calc}% http://ctan.org/pkg/calc
\usepackage[acronym, nonumberlist]{glossaries} % https://www.overleaf.com/learn/latex/Glossaries
\usepackage{biblatex}
%\usepackage{pdfpages}
\usepackage{float}
\usepackage{minitoc}
\usepackage{pdflscape}
\usepackage{hyperref} % https://ctan.org/pkg/hyperref
\usepackage{comment}


\input{I-config}

% Glossary
\newacronym{gcd}{GCD}{Greatest Common Divisor}
\newacronym{lcm}{LCM}{Least Common Multiple}


\addbibresource{../literature_review.bib}


% some package need to be loaded last in preamble
\usepackage{subfiles}

\begin{document}



\pagestyle{empty}
%\input{Preliminaries/1-titlepages}

\begin{center}
\vspace*{15pt}\par
\setstretch{1}
% \hrule
% \vspace{10pt}\par
\begin{spacing}{1.8}
%% you can replace by \MakeUppercase if you want uppercase
{\Large\bfseries\MakeLowercase{\capitalisewords{Cavitation Erosion of Blended Stellite Alloys}}}\\
\end{spacing}
% \hrule
% This thesis is composed of \numberVolume volumes. This one is the number \actualVolume.

\vspace{40pt}\par
\includegraphics[width=140pt]{Figures/logo.png}\\
\vspace{40pt}\par

{\itshape\fontsize{15.5pt}{19pt}\selectfont by\\}\vspace{15pt}\par

{
  \Large Vishakh Pradeep Kumar
  % , \distinction
}\vspace{55pt}\par

{
\large Submitted for the degree of \\ \vspace{8pt} \Large\slshape MSc. Mechanical Engineering\\
}

\vspace{35pt}\par

{\scshape\setstretch{1.5} School of Engineering and Physical Sciences\\ Heriot-Watt University\\
}

\vspace{50pt}\par


{\large April 2024}

%\vfill

%\begin{flushleft}
%\setstretch{1.4}\small
%The copyright in this thesis is owned by the author. Any quotation from the thesis or use of any of the information contained in it must acknowledge this thesis as the source of the quotation or information.
%\end{flushleft}
\end{center}


\clearpage
% % remove this line if you don't want pagination on preliminary pages
% % also read the comment below, for table of content and other


\pagestyle{preliminary}
%\input{Preliminaries/2-abstract}

\begin{center}
\LARGE\textbf {Abstract}
\end{center}
\vspace{5pt}

\noindent
Cavitation erosion process is a multifaceted phenomenon that depends not only on the strength characteristics of cavitating bubbles but also on the erosion resistance of materials to the cavitation energy imparted upon them. The loss of material due to cavitation leads to degradation in performance


The aim of this MSc project is to investigate the resistance of blended stellite alloys to cavitation erosion. The cavitation phenomena is simulated by ultrasonic vibrating probes, or sonicators, located at fixed gap from the material.

The synergistic effect existing between cavitation erosion and corrosion erosion is investigated with the help of in-situ electrochemical measurements of corrosion.


% Describe the apparatus
Experiments are to be conducted using an ultrasonic vibratory horn, with fixed frequency 20 kHz and variable peak to peak amplitude.

% Describe the need for scanning electron microscopy
Scanning electron microscopy is to be used to characterize the microstructural characteristics of the cavitated sample surfaces, as well as cross sections of the surface directly underneath cavitation.

%In accordance with the Academic Regulations the thesis must contain an abstract preferably not exceeding 200 words, bound in to precede the thesis. The abstract should appear on its own, on a single page.  The format should be the same as that of the main text. The abstract should provide a synopsis of the thesis and shall state clearly the nature and scope of the research undertaken and of the contribution made to the knowledge of the subject treated. There should be a brief statement of the method of investigation where appropriate, an outline of the major divisions or principal arguments of the work and a summary of any conclusions reached. The abstract must follow the Title Page.


\clearpage


%\includepdf[pages=-]{Preliminaries/5-declaration.pdf}
%{
%    \setstretch{1}
%    \hypersetup{linkcolor=black}
%    \tableofcontents
%    \listoftables % optional
%    \listoffigures % optional
%    \glsaddall % this is to include all acronym. You can do a sort of citation for acronym and include only the one you use, Look at the glossary package for details.
%    \printnoidxglossary[type=\acronymtype, title=Glossary] % optional
%    %% put your publications in BibMine.bib
%    %% They will be displayed here
%    \begin{refsection}[BibMine.bib]
%    \DeclareFieldFormat{labelnumberwidth}{#1}
%    \nocite{*}
%    \printbibliography[omitnumbers=true,title={List of Publications}]
%    \end{refsection}
%}




\clearpage

\pagestyle{chapter}
\chapter{Project Proposal}
\chaptermark{Project Proposal} % optional for veryy long chapter, you can rename what appear in the header

%\section{Start}
%\subsection{Basic Info}
%\subsubsection{This Is A Subsubsection}
%Subsubsection should not be numbered, nor indicated in the table of contents. The config options are tocdepth and secnumdepth, you can find them in the config file. 




% Describe why there is a need to look from both the fluid and solid perspective
Cavitation erosion is a complex phenomenon that results from hydrodynamic elements and material characteristics \cite{Franc2004265}.

%# Hydrodynamic POV
From a hydrodynamic standpoint, cavitation erosion results from the formation of and subsequent collapse of vapor bubbles within a fluid medium, due to the local pressure reaching the saturated vapor pressure (due to pressure decrease (cavitation) or temperature increase (boiling)). When these bubbles implode, they emit heat, shockwaves, and high-speed microjets that can impact adjacent solid surfaces, leading to damage to the surface and removal of material due to the accumulation of damage following numerous cavitation events.

The required pressure drop required by cavitation could be provided by the propagation of ultrasonic acoustic waves and hydrodynamic pressure drops, such as constrictions or the rotational dynamics of turbomachinery \cite{GEVARI2020115065}.

% Now do the materials POV
The resultant stress levels, which range from 100 - 1000 MPa, can surpass material resistance thresholds, including yield strength, ultimate strength, or fatigue limit, leading to material removal from the surface and subsequent degradation of industrial sysytems. The high strain rate in cavitation erosion makes it rather comparable to explosions or projectile impacts, albeit with very limited volume of deformation and repeated impact loads. The plastic deformation results in progressive hardening, crack propagation, and local fracture and removal of material, with the damage being a function of intensity and frequency of vapor bubble collapse. The selection of more resistent materials requires investigation of material response to cavitation stresses, with the mechanism of erosion being of particular interest. The resulting reduction of performance \& service life and the increased maintenance and repair costs motivate research into understanding how materials respond to the impact of a cavitating material. Cavitation erosion is a major problem in hydroelectric power plants \cite{Romo201216}, Francis turbines \cite{Kumar2024}, nuclear power plant valves \cite{Kim200685, Gao2024}, condensate and boiler feedwater pumps \cite{20221xix}, marine propellers \cite{Usta2023}, liquid-lubricated journal bearings \cite{Cheng2023}, and pipline reducers \cite{Zheng2022, Chen201442, Mokrane2019}.
% Stellites
Stellite alloys consist of a cobalt (Co) matrix with solid-solution strengthening of chromium (Cr) and tungsten(W)/moblybdenum(Mo), and hard carbid phases (Co, Cr, W, and/or Mo carbides) \cite{Shin2003117, Crook1992766, Desai198489, Youdelis1983379}. The matrix provides execelent high-temperature performance, while the carbides provide strength, wear resistance and resistance to crack propagation \cite{Ahmed2021, Crook199427}.

% Applications
Stellites are typically used for wear-resistant surfaces in lubrication-starved, high temperature or corrosive environments \cite{Zhang20153579, Ahmed2023, Ahmed20138, Frenk199481, Song1997291}, such as in the nuclear industry \cite{McIntyre1979105, Xu2024, Gao2024}, oil \& gas \cite{Teles2024, Sotoodeh2023929}, marine \cite{Song2019}, power generation \cite{Ding201797}, and aerospace industries \cite{Ashworth1999243}. Hot Isostatic Pressing (HIP) consolidation of Stellite alloys offers significant technological advantages for components operating in aggressive wear environments \cite{Ahmed20138, Ahmed201470, Ashworth1999243, Yu20071385}. Yu et al \cite{Yu2007586, Yu20091} note that HIP consolidation results in superior impact and fatigue resistance over cast alloys.

% Understanding the matrix phase
Understanding the cobalt phase is crucial for studying structural changes in Co-based alloys widely used in industry. Cobalt and Co-Cr-Mo alloys undergo thermally induced phase transformation from the high temperature face-centered cubic (fcc) $\gamma$ phase to low temperature hexagonal close-packed (hcp) $\epsilon$ phase at 700 K and strain induced fcc-hcp transition through maretensitic-type mechanism (partial movement of dislocations) \cite{HUANG2023106170}. At ambient conditions, the metastable FCC retained phase in stellites can be transformed into HCP phase by mechanical loading, although any HCP phase is completely transformed into a FCC phase between 673 K and 743 K \cite{DUBOS2020128812}; the metastable fcc cobalt phase in stellite alloys \cite{Rajan19821161} absorbs a large part of imparted energy under the mechanical loading of cavitation erosion. The fcc to hcp transition is related to the very low stacking fault energy of the fcc structure (7 mJ/m2) \cite{Tawancy1986337}. Solid-solution strengthening leads to increase of the fcc cobalt matrix strength (due to distortion of the atomic lattice with the additino of elements of different atomic radiuses), decrease of low stacking fault energy \cite{Tawancy1986337} due to the adjusted electronic structure of the metallic lattice, and inhibition of dislocation cross slip. Given that dislocation cross slip is the main deformation mode in imperfect crystals at elevated temperature, as dislocation slip is a diffusion process that is enhanced at high temperature, this leads to high temperature stability \cite{LIU2022294}. The addition of nickel (Ni), iron (Fe), and carbon (C) stabilize the fcc structure of cobalt (a = 0.35 nm), while chromium (Cr) and tungsten (W), stabilize the hcp structure (a = 0.25 nm and c = 0.41 nm), although Cr and W increases hot corrosion resistance \cite{Vacchieri20171100, Tawancy1986337}.



% How well was the novelty of the project expressed?
% Me jerking off to the novelty of my thesis
To date, academic research pertaining to cavitation erosion specifically on HIP'd stellite alloys appears to be absent from the existing literature, which underscores the need for exploration.
Moreover, the complexity introduced by blended stellite alloys in the context of cavitation erosion in corrosive enironments adds another layer of intrigue to this research endeavor. By analyzing the interactions between alloy composition, microstructure, and cavitation erosion behavior, this thesis aims to fill a critical gap in the current understanding of material performance under cavitation erosion conditions.




\label{Bibliography}
\printbibliography[title={References}, heading=bibintoc, resetnumbers=true]


\end{document}
