(TeX-add-style-hook
 "I-config"
 (lambda ()
   (TeX-add-symbols
    '("fref" 1)
    '("tref" 1)
    '("eref" 1)
    "headingBaseline"
    "headingBaselineDiv")
   (LaTeX-add-lengths
    "chapterFontSize"
    "sectionFontSize"
    "subsectionFontSize"
    "chapterBaseline"
    "sectionBaseline"
    "subsectionBaseline"))
 :latex)

