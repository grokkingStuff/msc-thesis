(TeX-add-style-hook
 "Thesis"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("report" "12pt" "a4paper")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("glossaries" "acronym" "nonumberlist")))
   (TeX-run-style-hooks
    "latex2e"
    "I-config"
    "report"
    "rep12"
    "graphicx"
    "fancyhdr"
    "inputenc"
    "fontenc"
    "setspace"
    "mathptmx"
    "slantsc"
    "titlesec"
    "mfirstuc"
    "calc"
    "glossaries"
    "biblatex"
    "float"
    "minitoc"
    "pdflscape"
    "hyperref"
    "comment"
    "subfiles")
   (LaTeX-add-labels
    "Bibliography")
   (LaTeX-add-bibliographies
    "../literature_review"))
 :latex)

