(TeX-add-style-hook
 "risk"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("geometry" "a4paper" "margin=1cm" "landscape")))
   (TeX-run-style-hooks
    "latex2e"
    "minimal"
    "minimal10"
    "geometry"
    "tikz"
    "comment"))
 :latex)

