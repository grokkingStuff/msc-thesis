(TeX-add-style-hook
 "eds_table"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("standalone" "tikz" "border=5mm")))
   (TeX-run-style-hooks
    "latex2e"
    "standalone"
    "standalone10")
   (TeX-add-symbols
    '("SyntheticElem" 4)
    '("NaturalElem" 4)
    '("ElemLabel" 4)))
 :latex)

