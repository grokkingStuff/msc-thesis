\acswitchoff 
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Aims and Objectives}{3}{section.1.1}%
\contentsline {section}{\numberline {1.2}Resources}{5}{section.1.2}%
\contentsline {section}{\numberline {1.3}Risks}{6}{section.1.3}%
\contentsline {subsection}{\numberline {1.3.1}Experimental setup complexity risks}{6}{subsection.1.3.1}%
\contentsline {subsection}{\numberline {1.3.2}Health \& Safety risks}{7}{subsection.1.3.2}%
\contentsline {section}{\numberline {1.4}Beneficiaries \& Stakeholders}{7}{section.1.4}%
\contentsline {chapter}{\numberline {2}Literature Review}{8}{chapter.2}%
\contentsline {section}{\numberline {2.1}Measuring cavitation erosion through ASTM G32}{9}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}Effect of distance between sonotrode and specimen}{9}{subsection.2.1.1}%
\contentsline {subsection}{\numberline {2.1.2}Effect of liquid temperature}{10}{subsection.2.1.2}%
\contentsline {section}{\numberline {2.2}Stellite}{11}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Matrix phase}{12}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}Carbide phase}{14}{subsection.2.2.2}%
\contentsline {subsection}{\numberline {2.2.3}Blended Stellite Alloys}{14}{subsection.2.2.3}%
