%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                     Heriot-Watt University Thesis Template  
%   Created by : Majed Al Saeed
%   Date:        June 2012
%   Department of Computer Science
%   Heriot-Watt University
%
%   Updated by: Alexandre Coates
%   Date: February 2021  
%   Institute of Photonics and Quantum Sciences
%   Heriot-Watt University
%
%   Used by: Vishakh Pradeep Kumar
%   Date: April 2024  
%   School of Engineering and Physical Sciences
%   Heriot-Watt University
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[a4paper,12pt]{report}

%==============================title page info=================================

% Name of the author
\newcommand{\auth}{Vishakh Pradeep Kumar}
% Title of Thesis                
\newcommand{\thesistitle}{Cavitation Erosion of Blended Stellite Alloys}
% Degree  
\newcommand{\degree}{MSc. Mechanical Engineering}
% Date submitted
\newcommand{\supdate}{April 2024}            

%===================================packages===================================
\usepackage[top=20mm, bottom=20mm, left=40mm, right=20mm]{geometry}
\usepackage{setspace}
\usepackage{fancyhdr} %fancy headers
\usepackage{acro} %acronyms
\usepackage{graphicx} %graphics
%\usepackage{subfigure}%
%\usepackage{subcaption} %subcaptions for subfigures
\usepackage{xspace}
\usepackage{listings}
\usepackage{pdfpages}
\usepackage{alltt}
\usepackage{float} % to use [H] 
\usepackage[notindex, nottoc, notlof, notlot]{tocbibind} %table of content options
%\usepackage{lscape}    % if you want to use land scape in one paper 
                       %...\begin{landscape}\end{landscape}
\usepackage{amsmath} %mathematical environments for equations etc
\usepackage{amssymb} %mathematical symbols
\usepackage{amsthm} %for defining mathematical theorems
\usepackage{siunitx} %mainly for producing the degrees symbol using \ang{}, but contains SI units obviously
\usepackage{braket} %for BraKet notation
\usepackage{nicefrac} %a nice fraction, side by side rather than top to bottom
\usepackage{hyperref} %in-document links between references and sections/figures/equations
\usepackage{wrapfig} %figure placement
\usepackage[counterclockwise]{rotating}
\usepackage{booktabs} %better tables

\usepackage{graphicx}
\usepackage{xcolor}
\usepackage{multirow}
\usepackage{amsfonts}

\usepackage{cleveref} %more internal referencing behaviour, lets you use \cref instead of \ref, MUST BE AFTER HYPERREF
%\usepackage[utf8]{inputenc} %related to encoding %not needed as of 2018/9 I think
\usepackage{parskip} % OPTIONAL - removes paragraph indentation
\usepackage[colorinlistoftodos]{todonotes} % to do notes, might help you keep track of things
\reversemarginpar
\setlength{\marginparwidth}{35mm}
%\reversemarginpar %force todonotes into the left margin, as it defaults to the right hand margin

\usepackage{enumitem}


%%% Citations %%%
\usepackage[backend=biber,style=numeric-comp, sorting=none]{biblatex} %using biblatex for citations, style is set to numeric-comp by default, and sorting is set to none so the bibliography prints references in the order of their appearance. See more details in the main document

\addbibresource{literature_review.bib} %add a source for references here
%if you want to load from multiple bib files then do \addbibresource{file1,file2,file3} - no spaces, and you may need the .bib or whatever you file extension is


%%% other things %%%
\graphicspath{{Figures/}{../Figures/}} %sets path for figures for two levels of nesting the fig folder

\setcounter{tocdepth}{3}  %set how many subsections your table of contents will show down to
\setcounter{secnumdepth}{3} %set how many levels deep your section numbering will go, 3 means going to subsubsection
\date{\today}

%=========================== define acronyms ===============
%list the command to type, the short version, and the long version at a minimum
%for more advanced features like grouping, tagging or setting capitalisation, see the acro documentation on CTAN
%\DeclareAcronym{lol}{short = lol, long = laughing out loud}
%\DeclareAcronym{www}{short = WWW , long  = World-Wide Web}
%\DeclareAcronym{wys}{short=WYSIWYG, long = What you see is what you get}
%\DeclareAcronym{woodchuck}{short = HMWWAWCIAWCCW, long= How much wood would a woodchuck chuck if a woodchuck could chuck wood?}
%\DeclareAcronym{opt}{short = Optional, long = Including a list of used terms/acronyms is totally optional}
%\DeclareAcronym{jau}{short = JAU, long = Join a Union}
%\input {Acronyms} %or you can list your acronyms in a separate file, but it is more work to sort out with subfiles


\usepackage{subfiles} %this need to be the LAST package you include
%===================================Document===================================
\begin{document}
\doublespacing

%==================================Title page==================================
\pagestyle{empty}
\begin{center}
\begin{spacing}{2}
{\large{\ \\  \vspace{1.5cm}\textbf{\MakeUppercase{Research Proposal}}}}\\
{\Large{\ \\  \vspace{0.05cm}\textbf{\MakeUppercase{\thesistitle}}}}\\
\end{spacing}
\vfill
{\Large\textit{by}}\\\vspace{0.2cm}
{\Large\upshape{\auth}}\\\vspace{1.0cm}
\includegraphics[width=5cm]{HW_shield}\\
\vspace{1cm}
{\large Submitted for the degree of \\ \degree}\\
\vspace{1cm}
{\large\textsc{School of Engineering and Physical Sciences}\\
\textsc{Heriot-Watt University}}\vfill
{\large{\supdate}}
\end{center}
%===================================Abstract=================================
\clearpage
\begin{center}
\LARGE\textbf {Abstract}
\end{center}
\vspace{1cm}

\begin{spacing}{1}
\noindent
%Write the abstract here.
%In accordance with the Academic Regulations the thesis must contain an abstract preferably not exceeding 200 words, bound in to precede the thesis. The abstract should appear on its own, on a single page.  The format should be the same as that of the main text. The abstract should provide a synopsis of the thesis and shall state clearly the nature and scope of the research undertaken and of the contribution made to the knowledge of the subject treated. There should be a brief statement of the method of investigation where appropriate, an outline of the major divisions or principal arguments of thework and a summary of any conclusions reached. The abstract must follow the Title Page.

Cavitation erosion is a complex phenomenon influenced by the strength properties of cavitating bubbles and material resistance, leading to performance degradation through material loss. This research endeavors to evaluate the resistance of blended stellite alloys to cavitation erosion. Simulation of cavitation phenomena will be achieved using ultrasonic vibrating probes positioned consistently from the material. The study will investigate the synergistic interplay between cavitation and corrosion erosion through in-situ electrochemical measurements. Experimental procedures will involve an ultrasonic vibratory horn operating at a fixed frequency of 20 kHz, with adjustable peak-to-peak amplitude. Microstructural characterization of cavitated sample surfaces and underlying cross-sections affected by cavitation will be conducted using scanning electron microscopy.


\end{spacing}
%==================================frontmatter================================
\clearpage
\pagestyle{plain}
\clearpage\pagenumbering{roman}

%\noindent
%{\LARGE\textbf{Acknowledgements}}
%\vspace{1cm}

%\begin{spacing}{1}
%\noindent
%You can write whatever you want to in the Acknowledgements, I have seen thanks to videogames, rubber ducks, takeaway restaurants, Karl Marx and so on. Some people even go as far as to put down things or people that got in the way of their thesis. If you consider going that route, keep things civil! Still, this is your space, there's no real guidelines, include a fancy quote then several paragraphs about ghosts if you like, or maybe a poem that speaks to you. Have a think about it.

%After this comes the mandatory table of contents.

%\textbf{Lists of Tables and Figures, Glossary, List of Publications by the Candidate}

%It is \textit{optional} to provide these lists. If provided, then they should start on the page following the table of contents and be in the order: Tables, Figures, Glossary (list of abbreviations), Publications.

%Items in lists of Tables and Figures should be in the order in which they occur in the text.
%\end{spacing}

\tableofcontents
%\listoftables %optional
%\listoffigures %optional%
%\printacronyms %OPTIONAL prints used acronyms wherever you put this
%===================================headings=================================
\clearpage
\pagestyle{fancy}
\pagenumbering{arabic}
\fancyhead{}
\lhead{\slshape \leftmark} 
\cfoot{\thepage}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.0pt}
\renewcommand{\chaptermark}[1]{\markboth{\chaptername\ \thechapter:\ #1}{}}
%===================================Chapters================================

\input{Chapter_Introduction}
\input{Chapter_LitReview}
%\subfile{Chapter_Archive}
%\subfile{Chapter_Background}
%\subfile{Chapter_Design}
%\subfile{Chapter_Conclusion}

%===================================Appendix================================
\appendix
\renewcommand{\chaptermark}[1]{\markboth{Appendix \thechapter.\ #1}{}}
%\subfile{Chapters/Appendix1}

%=================================Bibliography================================
%If you just need super simple references you can remove all the biblatex stuff and have this for a lump of references at the end, but it's less flexible than biblatex. Remember, you can just remove all of the refsections and biblatex will work with just a \printbibliography command wherever you'd like. 

%\bibliographystyle{abbrv}
%\bibliography{Bibliography.bib}

%print bibliography for whole thesis (not obligatory, you can do per-chapter bibliographies if you want)
%\printbibliography[section=1,heading=subbibliography, title= Introduction] %print references for the 1st refsection, custom title for references
%\printbibliography[section=2,heading=subbibliography, title = some random custom title] %print references for the 2nd refsection,

%\printbibheading %print the heading that say BIBLIOGRAPHY
\printbibliography % print ALL references

%\printbiblist
%the sorting order of your bibliography is determined by the arguments in the square brackets when importing biblatex, can sort by year or name if preferred
\end{document}


