%\documentclass[../HWThesis.tex]{subfiles} %Copy this at the top of each subfile, then you can render the .tex file on its own
%\begin{document}
%\begin{refsection}
\chapter{Literature Review}
\label{ch:literature_review}


% Describe why there is a need to look from both the fluid and solid perspective
Cavitation erosion is a complex phenomenon that results from hydrodynamic elements and material characteristics \cite{Franc2004265}. When components are exposed to sustained cavitation erosion, the component surface is degraded and material is progressively lost.

% Hydrodynamic POV
From a hydrodynamic standpoint, cavitation erosion results from the formation of and subsequent collapse of vapor bubbles within a fluid medium, due to the local pressure reaching the saturated vapor pressure (due to pressure decrease (cavitation) or temperature increase (boiling)). When these bubbles implode, they emit heat, shockwaves, and high-speed microjets that can impact adjacent solid surfaces, leading to damage to the surface and removal of material due to the accumulation of damage following numerous cavitation events \cite{Yu2024771, Niedzwiedzka201671, Micu2017894}. The required pressure drop required by cavitation could be provided by the propagation of ultrasonic acoustic waves and hydrodynamic pressure drops, such as constrictions or the rotational dynamics of turbomachinery \cite{GEVARI2020115065}. Impurities in the fluid, such as solid particles and nanobubbles with a radius of 500nm can significantly reduce the cavitation threshold leading to increased cavitation intensity \cite{Bai2020}. When these bubbles collapse near walls, the concentration of energy on very small areas of the wall result in high stress levels on the wall \cite{Karimi19861}.


% Now do the materials POV
The resultant stress levels, which range from 100 - 1000 MPa, can surpass material resistance thresholds, including yield strength, ultimate strength, or fatigue limit, leading to material removal from the surface and subsequent degradation of industrial systems \cite{Yu2024771, Niedzwiedzka201671, Micu2017894}. The high strain rate in cavitation erosion makes it rather comparable to explosions or projectile impacts, albeit with very limited volume of deformation and repeated impact loads \cite{Meged2002914, Hattori2010855, Steller2021, Steller2020, Meged2015262, FortesPatella2013205}. The plastic deformation results in progressive hardening, crack propagation, and local fracture and removal of material, with the damage being a function of intensity and frequency of vapor bubble collapse \cite{KARIMI19871, Meng1995443, Berchiche2002601}. The selection of more resistent materials requires investigation of material response to cavitation stresses, with the mechanism of erosion being of particular interest \cite{Meged2003277, Soyama200427, Meged200642}. The resulting reduction of performance \& service life and the increased maintenance and repair costs motivate research into understanding how materials respond to the impact of a cavitating material.



\section{Measuring cavitation erosion through ASTM G32}

% Let's describe the ultrasonic cavitation setup and go deeper
% Why is thin layer stuff so important?
The ASTM G32 standard defines the study of cavitation performance of materials by placing an ultrasonic sonotrode above a stationary specimen, forming a thin liquid layer between the two solid walls, as seen in Figure \ref{fig:ASTMG32_fig}. The sonotrode horn emits an acoustic wave into the fluid and causes cavitation when the pressure amplitude is sufficiently high. Due to the reflection and superposition of ultrasound in the thin liquid layer, the intensity of cavitating bubbles is increased, leading to accelerated cavitation erosion \cite{ASTMG32, Bai2020, Hammitt1980}.

\begin{figure}[h!]
  \centering
  \label{fig:ASTMG32_fig}
\includegraphics[width=0.98\textwidth]{Figures/ASTMG32_important_parameters.png}
\caption{Important parameters of experimental apparatus from ASTM G32. From \cite{ASTMG32}}
\end{figure}

\subsection{Effect of distance between sonotrode and specimen}
Endo et al \cite{Endo1967229} found that the extent of damage depends upon the thickness of the thin liquid layer, Kikuchi et al \cite{Kikuchi1985211} find that the extent of damage is a function of the reciprocal of the thickness of the liquid layer. For thicknesses $h < 0.5mm$, numerous bubbles coalese into several large bubble clusters in contact with the horn tip and the staionary specimen, while for thicknesses $h > 0.5mm$, the numerous bubbles produced are isolated \cite{Me-Bar1996741,Abouel-Kasem201221702, Wu201775}.

\subsection{Effect of liquid temperature}
The test water temperature affects the degree of cavitation erosion \cite{Singer1979147, Ahmed1998119}, with mass loss rate initially increasing with increase in temperature, peaking at an optimum temperature $T_m$, then decreasing with further increase in temperature \cite{Peng2020}, with bulk liquid temperatures above 50 C not altering erosion rate significantly \cite{Singer1979147, Nagalingam20182883}. However, it must be noted that the temperature of the liquid film between the ultrasonic tip and sample rises rapidly, regardless of the bulk liquid temperature \cite{Endo1967229, Abouel-Kasem201221702}, with maximum erosion rates observed with film temperatures at temperatures 30-35 C \cite{Singer1979147, Priyadarshi2023}.


\section{Stellite}

% Stellites
Stellite alloys belong to the cobalt-chromium family, with the addition of tungsten or molybdenum as the main alloying elements.
The matrix in stellite alloys consist of cobalt (Co) with solid-solution strengthening of a substantial amount of chromium (Cr) and tungsten(W)/moblybdenum(Mo), resulting in high hardness \& strength at high temperature, with carbide precipitations (Co, Cr, W, and/or Mo carbides) adding strength and wear resistance \cite{Shin2003117, Crook1992766, Desai198489, Youdelis1983379, Ahmed2021, Crook199427}. Stellites are typically used for wear-resistant surfaces in lubrication-starved, high temperature or corrosive environments \cite{Zhang20153579, Ahmed2023, Ahmed20138, Frenk199481, Song1997291}, such as in the nuclear industry \cite{McIntyre1979105, Xu2024, Gao2024}, oil \& gas \cite{Teles2024, Sotoodeh2023929}, marine \cite{Song2019}, power generation \cite{Ding201797}, and aerospace industries \cite{Ashworth1999243}.



The wear resistance of different stellite alloys manufactured by casting, forging, laser cladding, and hot isostatic pressing (HIP) has been investigated extensively, \cite{Opris2007581, Engqvist2000219, Antony198352, Crook1992766, Desai198489, Yang1995196, DeMolVanOtterloo19971225, Frenk199481, Ahmed20138, Yu2007586, KRELL2020203138, Yu2007586, KRELL2020203138}. Hot Isostatic Pressing (HIP) consolidation of Stellite alloys offers significant technological advantages for components operating in aggressive wear environments \cite{Ahmed20138, Ahmed201470, Ashworth1999243, Yu20071385}. Yu et al \cite{Yu2007586, Yu20091} note that HIP consolidation results in superior impact and fatigue resistance over cast alloys. The cavitation erosion of stellites has been investigated in experimental studies, as seen in Table \ref{tab:stellite}, \cite{Wang2023, Szala2022741, Mitelea2022967, Liu2022, Sun2021, Szala2021, Zhang2021, Mutascu2019776, Kovalenko2019175, E201890, Ciubotariu2016154, Singh201487, Hattor2014257, Depczynski20131045, Singh2012498, Romo201216, Hattori20091954, Ding201797, Guo2016123, Ciubotariu201698}, along with investigations into cobalt-based alloys \cite{Lavigne2022, Hou2020, Liu2019, Zhang20191060, E2019246, Romero2019581, Romero2019518, Lei20119, Qin2011209, Ding200866, Feng2006558}. However the cavitation erosion mechanism has not been fully established, particularly with the effect of microstructure due to different fabrication techniques, as seen in Figure \ref{fig:stellite_microstructure}.
In addition to the energy  absorbing effect of phase transformation of the cobalt matrix \cite{Feng2006558}, Heathcock et al \cite{Heathcock1981597} find that finer carbide structure leads to increased cavitation erosion resistance, an observation ratified by Garzon et al \cite{Garzon2005145}. Cavitation erosion of stellite coatings is improved in seawater, when compared to distilled water \cite{Hou2020}, likely due to the protective effect of chromium oxides inhibiting formation of erosion pits \cite{Liu2019}.



\begin{figure}[h!]
  \centering
  \label{fig:stellite_microstructure}
\includegraphics[width=0.98\textwidth]{Figures/microstructure.jpg}
\caption{Microstructure of Stellite alloys 1, 12, 6, \& 21 due to casting, welding, and HIP'ing. From \cite{KRELL2020203138}.}
\end{figure}


% Insert table of stellite compositions here
% Why are stellites OP at cavitation?
% Stellites have good CE resistance due to the low stacking fault energy of the cobalt fcc phase, which favors planar slip dislocations and increases the number of cycles that leads to fatigue failure.


\begin{table}[ht]
\centering
% To place a caption above a table
\caption{Operating parameters used in ASTM G32 tests on Stellite specimens}
\label{tab:stellite}
\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|}
\hline
\multirow{3}{*}{\rotatebox{-90}{\bf Indirect}} & \multirow{3}{*}{\rotatebox{-90}{\bf Water}}         & HIP'ed Stellite 6                                     & 50 & - & 0.5 & 1.5 & 24 & 2.09 & \cite{Szala2022741} \\
                                               &                                                     & $5 \times 10^{16} \frac{Mn^{+}}{cm^{2}}$ HIP'ed Stellite 6  & 50 & - & 0.5 & 1.5 & 24 & 2.07 & \cite{Szala2022741} \\
                                               &                                                     & $10 \times 10^{16} \frac{Mn^{+}}{cm^{2}}$ HIP'ed Stellite 6 & 50 & - & 0.5 & 1.5 & 24 & 1.88 & \cite{Szala2022741} \\
\hline
\hline
\multirow{10}{*}{\rotatebox{-90}{\bf Direct}}  & \multirow{3}{*}{\rotatebox{-90}{\bf Water}}         & LC Stellite 6                                         & 50 & 25 & - & 1   & 14 & 2.7   & \cite{Sun2021} \\
                                               &                                                     & SLD Stellite 6                                        & 50 & 25 & - & 1   & 14 & 0.77  & \cite{Sun2021} \\
                                               &                                                     & HVOF Stellite 21                                      & 25 & 25 & - & 0.5 & 8  & -     & \cite{Liu2022} \\ \cline{2-10}
                                               & \multirow{7}{*}{\rotatebox{-90}{\bf 3.5 wt\% NaCl}} & Stellite 728                                          & 50 & 25 & - & 5   & 50 & 1.012 & \cite{Wang2023} \\
                                               &                                                     & Stellite 6                                            & 50 & 25 & - & 5   & 50 & 2.841 & \cite{Wang2023} \\
                                               &                                                     & Stellite 6B                                           & 50 & 25 & - & 5   & 50 & 2.018 & \cite{Wang2023} \\
                                               &                                                     & HVOF Stellite 21                                      & 25 & 25 & - & 0.5 & 8  & -     & \cite{Liu2022} \\
                                               &                                                     & LC Stellite 6                                         & 50 & 25 & - & 1   & 14 & 0.044 & \cite{Zhang2021} \\
                                               &                                                     & SLD-1.0kW Stellite 6                                  & 50 & 25 & - & 1   & 14 & 0.017 & \cite{Zhang2021} \\
                                               &                                                     & SLD-1.0kW Stellite 6                                  & 50 & 25 & - & 1   & 14 & 0.017 & \cite{Zhang2021} \\
\hline
\multicolumn{2}{l}{} & & & & & & & & \\
\cline{1-3}
\multicolumn{3}{|l}{Peak to Peak Amplitude (\SI{}{\micro\metre}) } & & & & & & & \\
\cline{1-4}
\multicolumn{4}{|l}{Water Temperature (\SI{}{\celsius})} & & & & & & \\
\cline{1-5}
\multicolumn{5}{|l}{Standoff Distance (\SI{}{\milli\metre})} & & & & & \\
\cline{1-6}
\multicolumn{6}{|l}{Test Duration (hr)} & & & & \\
\cline{1-7}
\multicolumn{7}{|l}{Total Duration (hr)} & & & \\
\cline{1-8}
\multicolumn{8}{|l}{Terminal Erosion Rate for Eroded Area \SI{199}{\milli\metre\squared} (\SI{}{\milli\gram\per\hour})} & & \\
\cline{1-9}
\multicolumn{9}{|l}{References} & \\
\hline
\end{tabular}
\end{table}

Corrosion studies conducted on stellites find high corrosion resistance. The matrix is preferentially attacked, with the dissolution of $Co$ into $Co^{2+}$, while a surface layer comprised of chromium-rich oxides (Cr2O3 \& Cr(OH)3) prevents further corrosion in chloride-rich environments. Zhang et al find that stellite alloys with higher carbon content have less corrosion resistance \cite{Zhang20153579}. Malayoglu et al find improved erosion and corrosion resistance of HIP'ed Stellite 6 over cast Stellite 6, due to lessened removal of Co-rich matrix in HIP'ed material. \cite{MALAYOGLU2003181}. Mohamed et al report similar improved performance of HIP'ed Stellite 6 and attribute it to the fine grain size of carbides in HIP'ed materials \cite{Mohamed1999195}.




\subsection{Matrix phase}
% Understanding the matrix phase
% Understanding the cobalt phase is crucial for studying structural changes in Co-based alloys widely used in industry.
Cobalt and Co-Cr alloys undergo thermally induced phase transformation from the high temperature face-centered cubic (fcc) $\gamma$ phase to low temperature hexagonal close-packed (hcp) $\epsilon$ phase at 700 K and strain induced fcc-hcp transition through maretensitic-type mechanism (partial movement of dislocations) \cite{HUANG2023106170, Wang2023}. At ambient conditions, the metastable FCC retained phase in stellites can be transformed into HCP phase by mechanical loading, although any HCP phase is completely transformed into a FCC phase between 673 K and 743 K \cite{DUBOS2020128812, Liu2022}; the metastable fcc cobalt phase in stellite alloys \cite{Rajan19821161, Sun2021} absorbs a large part of imparted energy under the mechanical loading of cavitation erosion. The fcc to hcp transition is related to the very low stacking fault energy of the fcc structure (7 mJ/m2) \cite{Tawancy1986337, Szala2022741, DeMolVanOtterloo1997239}.

% Let's talk about the addition of other elements
Solid-solution strengthening leads to increase of the fcc cobalt matrix strength (due to distortion of the atomic lattice with the addition of elements of different atomic radii), and decrease of low stacking fault energy \cite{Tawancy1986337} due to the adjusted electronic structure of the metallic lattice. Dislocation motion in stellites is discouraged by solute atoms of Mo and W, due to the large atomic sizes. Given that dislocation cross slip is the main deformation mode in imperfect crystals at elevated temperature, as dislocation slip is a diffusion process that is enhanced at high temperature, this leads to high temperature stability \cite{LIU2022294}. In addition, nickel (Ni), iron (Fe), and carbon (C) stabilize the fcc structure of cobalt (a = 0.35 nm), while chromium (Cr) and tungsten (W), stabilize the hcp structure (a = 0.25 nm and c = 0.41 nm) \cite{Vacchieri20171100, Tawancy1986337}.

% Maybe get the size of atoms and show the mismatch?

\subsection{Carbide phase}

The amount and types of carbides dispersed in the stellite matrix are primarily determined by the carbon content, with higher carbon content encouraging carbides with higher C/M ratios, while the size of carbides is determined by the cooling rate \cite{Desai198489, DeMolVanOtterloo19971225}. Carbon content can be used to distinguish between different stellite alloys: high-carbon stellites designed for high wear resistance, abrasion, \& severe galling, medium-carbon (0.5 - 1.6\% wt) stellites used for high temperature service, and low-carbon (<0.5\% wt) stellites used primarily for corrosion resistance, cavitation, \& sliding wear \cite{kapoor2013microstructure, Szala2021}. Low-carbon stellites depend primarily of solid-solution strengthening for their mechanical properties. As the carbon content increases, the W/Mo content is usually also increased to prevent depletion of Cr from matrix solid solution strengthening \cite{Zhang20153579, Mohamed1999195}. Chromium is the predominant carbide former, with M7C3 and M23C6 phases, in addition to providing corrosion resistance and strength to the stellite  matrix \cite{Singh201487, Hattor2014257, Depczynski20131045}. Difference between the M7C3 and M23C6 phases is not readily visible under SEM. In tungsten-containing alloys, carbides of type M7C3 and M6C are formed in addition to the matrix. Ahmed et al report on the identification of intermetallic Co3W and Co7W6 phases through XRD, although these phases are not identified in SEM observations \cite{DeBrouwer1966141, Crook1990446, KRELL2020203138}.


\subsection{Blended Stellite Alloys}

Ahmed et al investigate the influence of the HIP'ing process on stellites \cite{Ahmed2021,Ahmed2017487,Ahmed201470,Ahmed201498,Yu20071385, Yu20091}, and conclude that HIP consolidation of Stellite alloys offers significant technological advantages for components operating in aggressive wear environments due to superior impact and fatigue resistance over cast alloys \cite{Ahmed20138, Ahmed201470, Ashworth1999243, Yu20071385}. In order to achieve unique microstructures from existing stellite alloys, Ahmed et al investigate the performance of blended alloys \cite{Ahmed2023,Ahmed2021}, which are formed through the consolidation of a mixture of two stellite powders.

% The heck is a blended stellite alloys
A blended stellite alloy is formed by hot isostatic pressing of a mixture of two stellite powders. The powders are created through gas atomization, in which a stream of liquid stellite alloy is disrupted and atomized into tiny molten droplets by a high-pressure inert gas flow \cite{Atkinson20002981, Ahmed2023, Yu2007, Ahmed2021}. The free-falling molten droplets rapidly solidify into spherical particles before being collected, forming high quality stellite powders with controllable size. The rapid cooling of the powder during atomization leads to reduced precipitation of carbides and supersaturation of the metallic matrix with other elements, as seen in the reduced proportion of carbide phases detected in the XRD performed on powders, compared to XRD of HIP'd samples. The mixing of powders is conducted in a powder hopper that ensures uniform distribution of powder mixtures \cite{Ahmed2023, Ahmed2021}. The HIP treatment was conducted at a temperature of 1200 C and a pressure of 100 MPa for a duration of 4 hours, resulting in full dense blended stellite alloys \cite{Li19872831, Ashworth2000351, Atkinson1991}. During the HIP'ing process, carbides are precipitated, in addition to reduction of supersaturation of the matrix \cite{Li19931345, Li19891645}. Depending on the composition of the stellite powders used, the blended alloys could possess uniform microstructure or regions that are similar to the constituent powders. This is due to the different diffusion rates of the added elements - carbon diffuses through the blended alloys while tungsten cannot diffuse due to its high atomic radius \cite{Speight1964683, Coble19704798, Ahmed2023, Ahmed2021}.

In summary, the literature review underscores the necessity for additional academic inquiry into the cavitation erosion resistance of HIP'ed stellite alloys, particularly focusing on the influence of composition on microstructure and cavitation erosion behavior. This thesis endeavors to address this gap in knowledge by conducting a comprehensive investigation.




% Novelty
% How well was the novelty of the project expressed?
%To date, academic research pertaining to cavitation erosion specifically on HIP'd stellite alloys appears to be absent from the existing literature.

% Novelty - Me jerking off to the novelty of my thesis
%Given the detrimental influence of voids and defects on cavitation erosion, the lack of academic investigation into cavitation erosion on HIP (Hot Isostatic Pressing) stellite alloys, underscores the need for further exploration. Moreover, the complexity introduced by blended stellite alloys in the context of cavitation erosion in corrosive enironments adds another layer of intrigue to this research endeavor. By analyzing the interactions between alloy composition, microstructure, and cavitation erosion behavior, this thesis aims to fill a critical gap in the current understanding of material performance under cavitation erosion conditions.


%\end{refsection}
%\end{document}
