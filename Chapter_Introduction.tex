%\documentclass[../HWThesis.tex]{subfiles} %Copy this at the top of each subfile, then you can render the .tex file on its own
%\begin{document}
%\begin{refsection}
\chapter{Introduction}
\label{ch:introduction}


% Describe why there is a need to look from both the fluid and solid perspective
%Cavitation erosion is a complex phenomenon that results from hydrodynamic elements and material characteristics \cite{Franc2004265}.
% Why is the above important?

Cavitation erosion occurs when vapor bubbles form and collapse within a fluid due to pressure reaching the vapor pressure threshold \cite{knapp1970cavitation, brennen1995cavitation, Lauterborn_Bolle_1975}. The implosion emits heat \cite{doi:10.1126/science.253.5026.1397}, shockwaves \cite{10.1115/1.4049933}, and microjets \cite{XIONG2022105899} that damage adjacent solid surfaces, leading to material removal due to cumulative cavitation events \cite{Franc2004265, karimi1986cavitation}. The resulting stress levels, as seen in Figure \ref{fig:cavitation_damage}, can exceed material thresholds, causing surface damage and system degradation \cite{Pereira1998}. Understanding material response to cavitation stresses is crucial for selecting resistant materials and minimizing maintenance costs.

\begin{figure}[h]
\centering
\includegraphics[width=0.98\textwidth]{Figures/the-damage-mechanism-of-cavitation.png}
\label{fig:cavitation_damage}
\caption{Damage mechanism of cavitation}
\end{figure}




Stellites are cobalt-chromium alloys that are typically used for surfaces in lubrication-starved, high temperature or corrosive environments \cite{Zhang20153579, Ahmed2023, Ahmed20138, Frenk199481, Song1997291}, such as in the nuclear industry \cite{McIntyre1979105, Xu2024, Gao2024}, oil \& gas \cite{Teles2024, Sotoodeh2023929}, marine \cite{Song2019}, power generation \cite{Ding201797}, and aerospace industries \cite{Ashworth1999243}. The wear resistance of different stellite alloys manufactured by casting, forging, laser cladding, and hot isostatic pressing (HIP) has been investigated extensively, \cite{Opris2007581, Engqvist2000219, Antony198352, Crook1992766, Desai198489, Yang1995196, DeMolVanOtterloo19971225, Frenk199481, Ahmed20138, Yu2007586, KRELL2020203138, Yu2007586, KRELL2020203138}.

The cavitation erosion of stellites has been investigated in experimental studies \cite{Wang2023, Szala2022741, Mitelea2022967, Liu2022, Sun2021, Szala2021, Zhang2021, Mutascu2019776, Kovalenko2019175, E201890, Ciubotariu2016154, Singh201487, Hattor2014257, Depczynski20131045, Singh2012498, Romo201216, Hattori20091954, Ding201797, Guo2016123, Ciubotariu201698}, along with investigations into cobalt-based alloys \cite{Lavigne2022, Hou2020, Liu2019, Zhang20191060, E2019246, Romero2019581, Romero2019518, Lei20119, Qin2011209, Ding200866, Feng2006558}.

Ahmed et al. investigate the impact of HIP'ing on stellite alloys, finding superior impact and fatigue resistance compared to cast alloys \cite{Ahmedd2021, Ahmed2017487,Ahmed201470,Ahmed201498,Yu20071385,Yu20091}. They also explored blended alloys formed by consolidating two stellite powders, resulting in unique microstructures influenced by the different diffusion rates of added elements. Depending on the composition of the stellite powders used, the blended alloys could possess uniform microstructure or regions that are similar to the constituent powders  \cite{Ahmed2023,Ahmed2021}. This is due to the different diffusion rates of the added elements - carbon diffuses through the blended alloys while tungsten cannot diffuse due to its high atomic radius \cite{Ahmed2023,Ahmed2021}.


%Ahmed et al investigate the influence of the HIP'ing process on stellites \cite{Ahmed2021,Ahmed2017487,Ahmed201470,Ahmed201498,Yu20071385, Yu20091}, and conclude that HIP consolidation of Stellite alloys offers significant technological advantages for components operating in aggressive wear environments due to superior impact and fatigue resistance over cast alloys \cite{Ahmed20138, Ahmed201470, Ashworth1999243, Yu20071385}. In order to achieve unique microstructures from existing stellite alloys, Ahmed et al investigate the performance of blended alloys \cite{Ahmed2023,Ahmed2021}, which are formed through the consolidation of a mixture of two stellite powders. During the HIP'ing process, carbides are precipitated, in addition to reduction of supersaturation of the matrix.

%The powders are created through gas atomization, in which a stream of liquid stellite alloy is disrupted and atomized into tiny molten droplets by a high-pressure inert gas flow. The free-falling molten droplets rapidly solidify into spherical particles before being collected, forming high quality stellite powders with controllable size \cite{Ahmed2023,Ahmed2021}.
%The rapid cooling of the powder during atomization leads to reduced precipitation of carbides and supersaturation of the metallic matrix with other elements, as seen in the reduced proportion of carbide phases detected in the XRD performed on powders, compared to XRD of HIP'd samples \cite{Ahmed2023,Ahmed2021}.
%The mixing of powders is conducted in a powder hopper that ensures uniform distribution of powder mixtures. The HIP treatment was conducted at a temperature of 1200 C and a pressure of 100 MPa for a duration of 4 hours, resulting in full dense blended stellite alloys \cite{Ahmed2023,Ahmed2021}.


% The use of blended stellite


% Stellite alloys can be manufactured by casting, deposition welding, laser cladding, forging, or hot isostatic pressing (HIP). These different processes result in different microstructures.

% Why are stellites OP at cavitation?
% Stellites have good CE resistance due to the low stacking fault energy of the cobalt fcc phase, which favors planar slip dislocations and increases the number of cycles that leads to fatigue failure.


% Novelty
% How well was the novelty of the project expressed?
%To date, academic research pertaining to cavitation erosion specifically on HIP'd stellite alloys appears to be absent from the existing literature.

% Novelty - Me jerking off to the novelty of my thesis
Given the detrimental influence of voids and defects on cavitation erosion, the lack of academic investigation into cavitation erosion on HIP (Hot Isostatic Pressing) stellite alloys, underscores the need for further exploration. Moreover, the complexity introduced by blended stellite alloys in the context of cavitation erosion in corrosive environments adds another layer of intrigue to this research endeavor. By analyzing the interactions between alloy composition, microstructure, and cavitation erosion behavior, this thesis aims to fill a critical gap in the current understanding of material performance under cavitation erosion conditions.



\section{Aims and Objectives}
% Were the aims of the project clearly expressed?
% Were they specific and measurable?
% Were they realistic?
% Were adequate timescales referred to?

Cavitation erosion impacts various industrial components, lowering their service life and increasing overall costs. In order to minimize damage \& losses due to cavitation, the mechanisms by which materials undergo cavitation erosion need to be understood. This work aims at identifying the most relevant factors to the cavitation erosion of base and blended stellite alloys, with a focus on how composition and microstructure affect cavitation resistance. The objectives of this work are to:

\begin{enumerate}

  \item \textbf{Design and develop} an experimental rig capable of accurately simulating cavitation erosion conditions in distilled water \& artificial seawater and achieving measurable \& replicable erosion rates, \textbf{by end of May}.
  \item \textbf{Quantify} cavitation erosion resistance of stellite materials in distilled water and artificial seawater by end of June
  \item \textbf{Investigate} the morphology, microstructure, chemical composition, and surface characteristics of eroded stellite samples \textbf{by end of July}.
        \begin{enumerate}
          \item \textbf{Acquire} Optical Microscopy images of eroded stellite samples at different stages of testing, in order to track changes of overall morphology of eroded surface.
          \item \textbf{Acquire} Scanning Electron Microscopy (SEM) images of eroded stellite samples to analyze the microstructural changes and phase composition resulting from cavitation erosion
          \item \textbf{Acquire} Energy Dispersive X-ray Spectrometry (EDS) images and scans to analyze the elemental composition of specific regions on the eroded stellite samples (elemental composition of matrix, carbides, and interfaces)
        \end{enumerate}
  \item \textbf{Develop} mathematical models for cavitation erosion of stellite alloys \textbf{by end of July}
        \begin{enumerate}
          \item \textbf{Investigate} the relationship between composition and previously reported structure-property relationships to cavitation erosion rates.
          \item \textbf{Assess} the applicability of parameter-models of cavitation erosion to experimental data of the cumulative mass loss of stellites.
        \end{enumerate}
  \item \textbf{Understand} the cavitation mechanism in stellite alloys and describe a phenomological model of CE in stellite alloys and provide actionable recommendations for enhancing cavitation resistance in stellite alloys
\end{enumerate}

Finite element simulations (FEA) and other numerical simulation techniques are outside the the scope of this thesis.

\clearpage
\section{Resources}
% In this section, you list what resources you require to do the project.
% Do you need lab access?
% Do you require access to archives?
% Do you need to establish a budget?
% How will you procure the required resources?

The designed rig will require the use of the following equipment
\begin{itemize}[noitemsep]
  \item Q500 Sonicator (existing)
  \item Vacuum Pump and Dessicator (purchased)
  \item Chilled Water Supply (existing)
  \item Coiled heat exchanger (purchased)
  \item Air Compressor (existing)
\end{itemize}

This work will require access to the following university laboratories.
\begin{itemize}[noitemsep]
  \item Energy Laboratory \\
        Location of relevant existing equipment (sonotrode, microscope, precision balance). There are two computers in the Energy Lab, the first to control the microscope and to handle image processing through ImageJ, and second for general purpose computing. The second computer has an automated backup, in addition to version control on all data stored.
  \item Chemical Laboratory \\
        Acetone is stored in Flammable Liquid Storage Cabinet in Chemical Lab, with purchase of more acetone available through vendors registered with Procurement. Distilled water is provided by Type 1 water purification system in the Chemical Laboratory.
  \item Fabrication \& Automotive Laboratory \\
        Access to tools for modification of equipment.
  \item Electronics Laboratory \\
        Access to soldering equipment for work on unpowered equipment.
\end{itemize}

In addition to the above, the following items are required:
\begin{itemize}[noitemsep]
  \item Specimens of Blended Stellite Alloys
        Samples are provided by Dr Rehan Ahmed.
  \item Material characterization equipment
        Access to SEM, EDS, and XRD facilities through an MoU w/ University of Sharjah,
\end{itemize}


\section{Risks}
The primary concern for this project revolves around time constraints and potential delays that were not adequately accounted for during the initial project planning phase.

\subsection{Experimental setup complexity risks}
Experimental setup could pose unexpected issues due to lack of planning. In order to mitigate the risk of unexpected design changes, the following strategies are to be employed

\begin{itemize}[noitemsep]

  \item Detailed Planning and Design in CAD \\
        The rig is to be designed in CAD to ensure all subsystems meet spatial, power, and I/O requirements.

  \item Expert Consultation \& Review \\
        The rig design is to be reviewed by supervisor and other expereinced researchers \& engineers. Feedback is to be recorded and designed altered to alleviate concerns. Identified people for review are Dr Rehan Ahmed, Dr Mohammed Al-Musleh, Muhsin Aykapaddatu

  \item Functionality/performance is not as expected or to specification \\
        Pilot testing of the rig to ASTM G32 standards using known materials (e.g., 316L stainless steel) will verify functionality and performance, comparing results with existing data.

  \item Documenting Procedures and Troubleshooting Protocols \\
        Detailed documentation of components and development of a Standard Operating Procedure (SOP) aligned with ASTM G32 standards will be maintained. Troubleshooting protocols will be established for unforeseen issues.

  \item Modular Design \& Redundancies \\
        The rig will feature a modular design for easy component adjustment. Spare parts will be readily available for quick replacement or repair, minimizing downtime.
\end{itemize}

\subsection{Health \& Safety risks}

\begin{itemize}
  \item Noise exposure \\
        The sonotrode emits high frequency noise that is
  \item Chemical Hazards
\end{itemize}

\section{Beneficiaries \& Stakeholders}

% Applications for CE research
Industry stakeholders, including manufacturers and technology providers, are likely to benefit from improved understanding of cavitation erosion in stellite alloys, enabling the development of more durable materials for applications in harsh environments, such as hydroelectric power plants \cite{Romo201216}, Francis turbines \cite{Kumar2024}, nuclear power plant valves \cite{Kim200685, Gao2024}, condensate and boiler feedwater pumps \cite{20221xix}, marine propellers \cite{Usta2023}, liquid-lubricated journal bearings \cite{Cheng2023}, pipline reducers \cite{Zheng2022, Chen201442, Mokrane2019}.

The project supervisor and academic faculty represent the primary stakeholders, whose critique will be necessary for attaining project \& academic objectives. Apart from serving as mentor, the project supervisor has provided rare specimens and leveraged inter-university connections to access material characterization facilities, enhancing the project's resources and capabilities. Other stakeholders are:

\begin{itemize}
  \item Peer Researchers: offer peer review and collaboration, in addition to being users of similar equipment. Undergraduate students are unlikely to be present during project duration, although they are likely to be end users of equipment after project close.
  \item Research Community contribute to understanding of cavitation erosion and benefit from data : Project outcomes generate data and contribute to understanding of cavitation erosion. Researchers and industrial partners
  \item Lab Manager: In addition to ensuring compliance with health and safety requirements, the lab manager is a doctoral student working on their research rig; their advice will be helpful.
\end{itemize}


%\end{refsection}
%\end{document}
