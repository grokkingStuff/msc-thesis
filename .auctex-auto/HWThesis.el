(TeX-add-style-hook
 "HWThesis"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("report" "a4paper" "12pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("geometry" "top=20mm" "bottom=20mm" "left=40mm" "right=20mm") ("tocbibind" "notindex" "nottoc" "notlof" "notlot") ("rotating" "counterclockwise") ("todonotes" "colorinlistoftodos") ("biblatex" "backend=biber" "style=numeric-comp" "sorting=none")))
   (add-to-list 'LaTeX-verbatim-environments-local "lstlisting")
   (add-to-list 'LaTeX-verbatim-environments-local "alltt")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "Chapter_Introduction"
    "Chapter_LitReview"
    "report"
    "rep12"
    "geometry"
    "setspace"
    "fancyhdr"
    "acro"
    "graphicx"
    "xspace"
    "listings"
    "pdfpages"
    "alltt"
    "float"
    "tocbibind"
    "amsmath"
    "amssymb"
    "amsthm"
    "siunitx"
    "braket"
    "nicefrac"
    "hyperref"
    "wrapfig"
    "rotating"
    "booktabs"
    "xcolor"
    "multirow"
    "amsfonts"
    "cleveref"
    "parskip"
    "todonotes"
    "enumitem"
    "biblatex"
    "subfiles")
   (TeX-add-symbols
    '("chaptermark" 1)
    "auth"
    "thesistitle"
    "degree"
    "supdate")
   (LaTeX-add-bibliographies
    "literature_review"))
 :latex)

